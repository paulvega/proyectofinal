<?php
$ruta_archivo = "notas.txt";

if (isset($_POST['codigo'])) {
    $datos[] = $_POST['codigo'];
    $datos[] = $_POST['nombre'];
    $datos[] = $_POST['nota1'];
    $datos[] = $_POST['nota2'];
    $datos[] = $_POST['nota3'];
    $datos[] = $_POST['nota4'];
    $flujo = fopen($ruta_archivo,"a") or die ("Error al crear");
    fwrite($flujo, implode('@', $datos));
    fwrite($flujo, "\n");
    fclose($flujo);
    $mensaje = "Datos guardados correctamente";
}
$registros = array();
$datos = file($ruta_archivo);
foreach ($datos as $dato) {
    $registros[] = explode('@', $dato);
}
?>
<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Registro de notas para Proyecto Final</title>
    </head>
    <body>
        <?php if (isset($mensaje)): ?>
            <h3><?php print $mensaje; ?></h3>
        <?php endif ?>
        <div class="container">
            <form method="post">
                Codigos  : <input class="form-control"  type="text" name="codigo" placeholder="Ingrese Código"><br>
                Nombres  : <input class="form-control form-control-sm" type="text" name="nombre" placeholder="Ingrese Nombre"><br>
                Nota1   : <input class="form-control form-control-sm" type="number" step="0.01" name="nota1" placeholder="Ingrese Nota1"><br>
                Nota2   : <input class="form-control form-control-sm" type="number" step="0.01" name="nota2" placeholder="Ingrese Nota2"><br>
                Nota3   : <input class="form-control form-control-sm" type="number" step="0.01" name="nota3" placeholder="Ingrese Nota3"><br>
                Nota4   : <input class="form-control form-control-sm" type="number" step="0.01" name="nota4" placeholder="Ingrese Nota4"><br>

                <input class="btn btn-primary" type="submit" value="Registrar datos">
            </form>
            <?php if (isset($registros)): ?>
            <div>
                <table class="table table-striped">
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Nota1</th>
                        <th>Nota2</th>
                        <th>Nota3</th>
                        <th>Nota4</th>
                    </tr>
                    <tbody>
                        <!-- RECORREMOS Y MOSTRAMOS NUESTROS DATOS RECUPERADOS. -->
                        <?php foreach ($registros as $registro): ?>
                            <tr>
                                <td><?php print $registro[0]; ?></td>
                                <td><?php print $registro[1]; ?></td>
                                <td><?php print $registro[2]; ?></td>
                                <td><?php print $registro[3]; ?></td>
                                <td><?php print $registro[4]; ?></td>
                                <td><?php print $registro[5]; ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <?php endif ?>
                    </div>

    </body>
</html>